# PBox Client

#### Description
Goal of this project is to build a client application for the P-Box application. This project consists of several parts:
1. Authentication service - Supports JWT authentication using `passport.js` library
2. Missions service library - supports CRUD operations upon messages

### How To Use

#### Run
To run this project in development mode, simply type `npm start` or `ng serve` if **Angular CLI** is globally installed, then, open your browser and navigate
to http://localhost:4500.
`src/envirmonments/envirmonment.ts` file will be used as the source of the environment variables.

You can also run an SSL version if necessary by typing `npm run ssl` - it will launch the client with a self-signed certificate.

#### Build for production
To build a version ready for production, simply type `npm run build` or `ng build --configuration=production`. It will compile the project using the environment variables from `src/envirmonments/envirmonment.prod.ts` file 

Feel free to view or fork and change the project if you need. For education purposes.

### References
* [Missions Service - microservice for saving missions](https://bitbucket.org/ienigmax/pboxex-missions/src/master/)
* [Main API (Auth Service)](https://bitbucket.org/ienigmax/pboxex-server/src/master/)
