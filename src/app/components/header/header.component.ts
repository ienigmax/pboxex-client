import {Component, OnInit} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {HttpClient} from '@angular/common/http';
import {Route, Router} from '@angular/router';
import {AuthenticationService} from '../../services/Authentication.service';
import {HeaderMessageService} from '../../services/header-message.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public isAuthenticated: boolean;
  public name: string;

  constructor(private cookieService: CookieService,
              private router: Router,
              private headerMessageService: HeaderMessageService,
              private authenticationService: AuthenticationService,
              private httpClient: HttpClient) {
    this.isAuthenticated = !!cookieService.get('token');

    if (this.isAuthenticated) {
      this.authenticationService.currentUserValue = JSON.parse(localStorage.getItem('sub'));
      this.name = this.authenticationService.currentUserValue.first_name;
      this.router.navigate(['pages', 'mission']);
    } else {
      this.router.navigate(['/login']);
    }
  }


  logout() {
    this.headerMessageService.changeMessage('logout');
  }

  ngOnInit(): void {
    this.headerMessageService.currentMessage$.subscribe((message) => {
      if (message === 'logout') {
        this.cookieService.delete('token', '/');
        this.isAuthenticated = false;
        this.authenticationService.currentUserValue = null;
        localStorage.removeItem('sub');
        this.router.navigate(['/']);
      } else if (message === 'login') {
        this.isAuthenticated = true;
      }
    });
  }

}
