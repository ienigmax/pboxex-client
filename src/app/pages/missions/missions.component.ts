import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Mission} from '../../models/mission.model';
import {ToastrService} from 'ngx-toastr';
import {MissionService} from '../../services/Mission.service';
import {first} from 'rxjs/operators';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-missions',
  templateUrl: './missions.component.html',
  styleUrls: ['./missions.component.scss']
})
export class MissionsComponent implements OnInit {

  public submitted = false;
  public count = 0;
  public busy = false;
  public list: Array<any> = [{
    key: 'aaa',
    value: {b: {c: ''}}
  }];
  public paginate: any = {
    limit: 10,
    page: 1
  };

  public missionFormShow = false;
  public form: FormGroup;
  public currentAction = 'Create';
  public currentMission: Mission;

  constructor(private formBuilder: FormBuilder,
              private toastrService: ToastrService,
              private spinner: NgxSpinnerService,
              private missionService: MissionService) {
    this.list.push();
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      key: ['', [Validators.required, Validators.minLength(1)]],
      value: ['', [Validators.required]],
    });
    this.getList();
  }

  get f() {
    return this.form.controls;
  }

  public action(type: string, entity?: Mission) {
    this.currentAction = type;

    if (type === 'Create') {
      this.toggleMissionForm(true);
      this.form.patchValue({key: '', value: '{}'});
    } else if (type === 'Update' || type === 'Get') {
      this.currentMission = entity;
      if (type === 'Update') {
        this.spinner.show();
        this.toggleMissionForm(true);
        this.form.patchValue({key: entity.key, value: entity.value});
        setTimeout(() => this.spinner.hide(), 500);   // Artificially
      }
    } else if (type === 'Delete') {

      if (confirm('Are tou sure that you want to delete this mission?')) {
        this.spinner.show();
        this.missionService.deleteEntity(entity.key)
          .subscribe(res => {
            this.toastrService.success('Mission deleted successfully');
            this.getList();
            console.log(res);
          }, err => {
            console.error(err);
            this.toastrService.error('Mission was not deleted');
          }).add(() => {
          this.spinner.hide();
        });
      }
    }
  }

  public toggleMissionForm(flag?: boolean) {
    this.missionFormShow = flag;
  }

  get checkJson() {
    try {
      const obj = this.form.value.value;
      const result = JSON.parse(obj);
      return !!result;
    } catch (e) {
      return false;
    }
  }

  public addMission() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    if (!this.checkJson) {
      return this.toastrService.error('invalid mission value, must be a JSON');
    }

    this.spinner.show();
    this.missionService.createEntity(this.f.key.value, this.f.value.value)
      .subscribe((response: any) => {
        this.toastrService.success('Added a new mission!');
        this.getList();
        // TODO - reload
      }, err => {
        console.error(err);
        this.toastrService.error('Creation failed');
      }).add(() => {
        this.spinner.hide();
        this.submitted = false;
    });
  }

  public editMission() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    if (!this.checkJson) {
      return this.toastrService.error('invalid mission value, must be a JSON');
    }

    this.spinner.show();
    this.missionService.updateEntity(this.currentMission.key, {key: this.f.key.value, value: this.f.value.value})
      .subscribe((response: any) => {
        this.toastrService.success('Mission saved');
        this.getList();
        this.missionFormShow = false;
      }, err => {
        console.error(err);
        this.toastrService.error('Creation failed');
      }).add(() => {
      this.spinner.hide();
      this.submitted = false;
    });
  }

  public getMission() {

  }

  public getList() {
    this.missionService.getList(this.paginate.limit, this.paginate.page)
      .subscribe(response => {
        this.list = response.list;
        this.count = response.count;
      }, err => {
        console.error(err);
      });
  }
}
