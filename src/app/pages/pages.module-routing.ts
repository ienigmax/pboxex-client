import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {MissionsComponent} from './missions/missions.component';
import {CommonModule} from '@angular/common';

const routes: Routes = [
  {
    path: 'mission',
    component: MissionsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule],
  exports: [RouterModule]
})
export class PagesModuleRouting {}
