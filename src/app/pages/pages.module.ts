import {NgModule} from '@angular/core';
import {MissionsComponent} from './missions/missions.component';
import {PagesComponent} from './pages.component';
import {PagesModuleRouting} from './pages.module-routing';
import {CommonModule} from '@angular/common';
import {NgbPaginationModule, NgbTooltip, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {WithCredentialsInterceptor} from '../Guards/with-creds.interceptor';
import {JwtInterceptor} from '../Guards/jwt.inteceptor';
import {ApiPrefixInterceptor} from '../Guards/api-prefix.interceptor';
import {NgxSpinnerModule} from 'ngx-spinner';

@NgModule({
  declarations: [PagesComponent, MissionsComponent],
  imports: [CommonModule, PagesModuleRouting, NgbTooltipModule,
    FormsModule, ReactiveFormsModule, NgbPaginationModule, NgxSpinnerModule],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: WithCredentialsInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ApiPrefixInterceptor, multi: true},

  ]
})
export class PagesModule {
}
