import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {AuthenticationService} from '../../services/Authentication.service';
import * as moment from 'moment';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  public busy = false;
  public form: FormGroup;
  public submitted = false;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private toastr: ToastrService,
              private authenticationService: AuthenticationService) {
  }

  get f() {
    return this.form.controls;
  }

  public submit() {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    this.authenticationService.register({
      // type: LoginTypes.LOCAL,
      first_name: this.f.first_name.value,
      last_name: this.f.last_name.value,
      email: this.f.email.value,
      password: this.f.password.value,
    })
      .subscribe(
        (response: any) => {
          this.toastr.success('Successfully signed up at: ' + moment().format('YYYY-MM-DD hh:mm:ss'));
        },
        error => {
          this.toastr.error(error.error.error);
        }).add(() => {
      this.busy = false;
    });
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

}
