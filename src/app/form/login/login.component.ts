import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {AuthenticationService} from '../../services/Authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import * as moment from 'moment';
import {CookieService} from 'ngx-cookie-service';
import {HeaderMessageService} from '../../services/header-message.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public busy = false;
  public form: FormGroup;
  public submitted = false;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private headerMessageService: HeaderMessageService,
              private cookieService: CookieService,
              private toastr: ToastrService,
              private authenticationService: AuthenticationService,
  ) {
  }

  get f() {
    return this.form.controls;
  }

  public submit() {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    this.authenticationService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.toastr.success('Successfully logged in at: ' + moment().format('YYYY-MM-DD hh:mm:ss'));
          this.cookieService.set('token', data.token);
          this.authenticationService.currentUserValue = data.user;
          localStorage.setItem('sub', JSON.stringify(data.user));
          this.headerMessageService.changeMessage('login');
          this.router.navigate(['pages', 'mission']);
        },
        error => {
          this.busy = false;
          console.error(error);
          this.toastr.error('Wrong email/password');
        });
  }


  ngOnInit(): void {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(1)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });

  }


}
