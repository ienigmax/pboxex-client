import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {NgbModule, NgbNav, NgbNavItem} from '@ng-bootstrap/ng-bootstrap';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoginComponent} from './form/login/login.component';
import {RegistrationComponent} from './form/registration/registration.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app.module-routung';
import {JwtInterceptor} from './Guards/jwt.inteceptor';
import {WithCredentialsInterceptor} from './Guards/with-creds.interceptor';
import {ToastrModule} from 'ngx-toastr';
import {ApiPrefixInterceptor} from './Guards/api-prefix.interceptor';
import {CookieService} from 'ngx-cookie-service';
import {PagesModule} from './pages/pages.module';
import { HeaderComponent } from './components/header/header.component';
import {CommonModule} from '@angular/common';
import {HeaderMessageService} from './services/header-message.service';
import { FooterComponent } from './components/footer/footer.component';

const isIE = window.navigator.userAgent.indexOf('MSIE ') > -1 || window.navigator.userAgent.indexOf('Trident/') > -1;

// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    HeaderComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    PagesModule,
    ToastrModule.forRoot(),
    AppRoutingModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: WithCredentialsInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ApiPrefixInterceptor, multi: true},
    NgbNav,
    NgbNavItem,
    CookieService,
    HeaderMessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
