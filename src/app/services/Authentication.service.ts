import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, EMPTY, Observable, of, throwError} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {User} from '../models/user.model';


@Injectable({providedIn: 'root'})
export class AuthenticationService {

  public currentUserValue: User;

  constructor(private http: HttpClient) {}

  public login(username: string, password: string) {
    return this.http.post<any>(`/auth/login`, {username, password})
      .pipe(
        tap(user => user));
  }

  public register(data: User) {
    console.log(data);
    return this.http.post<any>(`/auth/register`, data)
      .pipe(
        tap((res: { token: string }) => {
          return res;
        }),
        catchError(error => {
          console.error(error);
          return throwError(error);
        })
      );
  }

}
