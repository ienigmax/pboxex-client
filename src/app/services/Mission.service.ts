import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Mission} from '../models/mission.model';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MissionService {

  constructor(private http: HttpClient) {
  }

  public getList(limit: number, page: number) {
    return this.http.get<any>(`/mission/list?page=${page}&limit=${limit}`)
      .pipe(
        tap(response => response.data));
  }

  public getEntity(key: string) {
    return this.http.get<Mission>('/mission/' + key)
      .pipe(
        tap(response => response));
  }

  public createEntity(key: string, value: any) {
    return this.http.post('/mission/', { key, value })
      .pipe(
        tap(response => response));
  }

  public updateEntity(key: string, data: Mission) {
    return this.http.put('/mission/' + key, data )
      .pipe(
        tap(response => response));
  }

  public deleteEntity(key: string) {
    return this.http.delete('/mission/' + key)
      .pipe(
        tap(response => response));
  }
}
