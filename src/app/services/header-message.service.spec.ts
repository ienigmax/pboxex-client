import { TestBed } from '@angular/core/testing';

import { HeaderMessageService } from './header-message.service';

describe('HeaderMessageService', () => {
  let service: HeaderMessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HeaderMessageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
