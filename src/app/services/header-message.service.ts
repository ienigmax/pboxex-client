import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class HeaderMessageService {
  public messageSource = new BehaviorSubject<string>(localStorage.getItem(this.cookieService.get('token')));
  public currentMessage$ = this.messageSource.asObservable();
  constructor(private cookieService: CookieService) { }
  changeMessage(message: string) {
    this.messageSource.next(message);
  }
}
