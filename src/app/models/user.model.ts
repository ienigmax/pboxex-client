export interface User {
  uuid?: number;
  last_name: string;
  first_name: string;
  username?: string;
  email: string;
  password: string;
  token?: string;
}
