export const environment = {
  production: true,
  base_url: 'https://pbox.devmi.site',
  api_url: 'https://pbox.devmi.site/api/v1',
};
